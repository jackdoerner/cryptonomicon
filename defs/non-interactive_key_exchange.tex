\begin{definition}[\Nike~\cite{DH76,FHKP13, BZ14}] \label{def:nike}
A \Nike Scheme (\NIKE) is a tuple of \PPT algorithms, $(\mathsf{Setup},\allowbreak\mathsf{Publish},\allowbreak\mathsf{KeyGen})$ such that:
\begin{enumerate}
	\item Given a security parameter $\secparam$, a party count $\partycount$, and a group-size bound $g$, the $\mathsf{Setup}$ algorithm samples a public parameter set: $\pp \drawnfrom \mathsf{Setup}(1^\secparam,\partycount,g)$
	\item Given a set of public parameters $\pp$ and a user index $i$, the $\mathsf{Publish}$ algorithm samples a secret key $\sk_i$ and an associated public value $\pv_i$ for the user with index $i$: $(\pv_i,\sk_i) \drawnfrom \mathsf{Publish}_{\pp}(i)$
	\item Given a set of public parameters, a set of up to $g$ public values that are associated with the users indexed by $\vec{P}$, and the secret key of a single user $i$ (such that $i\in \vec{P}$), the $\mathsf{KeyGen}$ algorithm deterministically outputs a shared key $k$: $k\assign\mathsf{KeyGen}_{\pp}(i,\vec{P},\sk_i,\{\pv_j\}_{j\in \vec{P}})$
\end{enumerate}

A \nike scheme is \emph{correct} if, with overwhelmingly high probability, all secret keys generated for a particular set of users must be the same. Formally, we require that over the coins of $\pp \drawnfrom \mathsf{Setup}(1^\secparam,\partycount,g)$ and $(\pv_i,\sk_i) \drawnfrom \mathsf{Publish}_{\pp}(i)$ for all $i\in[\partycount]$, it must hold for all $\vec{P}\subseteq[\partycount]$ with $|\vec{P}|\le g$ and all $i,i'\in \vec{P}$ that
$$\Pr\left[\begin{aligned}
	&\mathsf{KeyGen}_{\pp}(i,\vec{P},\sk_i,\{\pv_j\}_{j\in \vec{P}})\\
	&\text{\qquad} = \mathsf{KeyGen}_{\pp}(i',\vec{P},\sk_{i'},\{\pv_j\}_{j\in \vec{P}})
\end{aligned}\right]\in1-\negl(\secparam)$$

The security of \nike schemes is defined relative to the \oracle $\mathsf{Game}$, which has the following behavior:
\begin{align*}
	&\mathsf{Game}_{b,\secparam,\partycount,g,\pp}(\op, \arg):\\
%
	&\parbox[t]{11cm}{\hangindent=2em \quad if $\op = \texttt{reg-hon}$ and $\arg = i$ such that $i\in[\partycount]$ and no record of the form $(\texttt{user}, i, *, *)$ exists in memory, then sample $(\pv_i\sk_i)\drawnfrom\mathsf{Publish}_\pp(i)$, store $(\texttt{user}, i, \sk_i, \pv_i)$ in memory, and output $\pv_i$.}\\
%
	&\parbox[t]{11cm}{\hangindent=2em \quad if $\op = \texttt{reg-cor}$ and $\arg = (i, \pv_i)$ such that $i \in [\partycount]$ and no record of the form $(\texttt{user}, i, *, *)$ or $(\texttt{user}, *, *, \pv_i)$ exists in memory, then store $(\texttt{user}, i, \bot, \pv_i)$ in memory.}\\
%
	&\parbox[t]{11cm}{\hangindent=2em \quad if $\op = \texttt{extract}$ and $\arg = i$ such that the record $(\texttt{user}, i, \sk_i, \pv_i)$ exists in memory, but there exists no record $(\texttt{tested},\vec{P})$ in memory such that $i\in\vec{P}$, then store $(\texttt{extracted},i)$ in memory and output $\sk_i$.}\\
%
	&\parbox[t]{11cm}{\hangindent=2em \quad if $\op = \texttt{reveal}$ and $\arg = \vec{P}$ such that $\vec{P}\subseteq[\partycount]$ and $|\vec{P}|\le g$ and there exist records of the form $(\texttt{user}, j, \sk_j, \pv_j)$ in memory for all $j\in \vec{P}$ such that for some $i\in \vec{P}$, $\sk_i\neq\bot$, but there exists no record $(\texttt{tested},\vec{P})$ in memory, then store $(\texttt{revealed}, \vec{P})$ in memory and output $\mathsf{KeyGen}_{\pp}(i, \vec{P}, \sk_i, \{\pv_j\}_{j\in \vec{P}})$.}\\
%
	&\parbox[t]{11cm}{\hangindent=2em \quad if $\op = \texttt{test}$ and $\arg = \vec{P}$ such that $\vec{P}\subseteq[\partycount]$ and $|\vec{P}|\le g$ and there exist records of the form $(\texttt{user}, j, \sk_j, \pv_j)$ such that $\sk_j\neq\bot$ in memory for all $j\in \vec{P}$, but there exists no records of the form $(\texttt{revealed}, \vec{P})$ or $(\texttt{tested}, \vec{P})$, nor any record of the form $(\texttt{extracted},i)$ for any $i\in\vec{P}$, then let $k\assign\mathsf{KeyGen}_{\pp}(\vec{P}_0, \vec{P}, \sk_{\vec{P}_0}, \{\pv_j\}_{j\in \vec{P}})$ if $b=0$ or $k\drawnfrom\bit^\secparam$ if $b=1$, store $(\texttt{tested},\vec{P})$ in memory, and output $k$.}\\
%
	&\parbox[t]{11cm}{\hangindent=2em \quad otherwise, output $\bot$.}
\end{align*}

A \nike scheme is \emph{adaptively secure} if for all \PPT adversarial algorithms $\mathcal{A}$ and all $n,g\in\NN^+$ such that $g\le n$ and $n$ is bounded by some polynomial in $\secparam$, it holds that
$$\left|\begin{aligned}
	&\Pr\left[\mathcal{A}^{\mathsf{Game}_{0,\secparam,\partycount,g,\pp}(\cdot, \cdot)}(1^\secparam,\partycount,g,\pp) = 1 : \pp \drawnfrom \mathsf{Setup}(1^\secparam,\partycount,g)\right]\\
	&\text{\quad}-\Pr\left[\mathcal{A}^{\mathsf{Game}_{1,\secparam,\partycount,g,\pp}(\cdot, \cdot)}(1^\secparam,\partycount,g,\pp) = 1 : \pp \drawnfrom \mathsf{Setup}(1^\secparam,\partycount,g)\right]
\end{aligned}\right|\in\negl(\secparam)
$$

Weaker notions of security are defined by modifying the $\mathsf{Game}$ oracle:
\begin{itemize}
	\item A \nike scheme is \emph{statically secure} if the $\mathsf{Game}$ oracle is modified such that the $\texttt{test}$ operation may only be queried on a specific hard-coded set of user indices $\vec{T}$, and the above probability statement is neglibible for all values of $\vec{T}$ such that $\vec{T}\subseteq[\partycount]$ and $|\vec{T}| = g$.
	\item A \nike scheme is \emph{semi-statically secure} if the $\mathsf{Game}$ oracle is modified such that for some particular hardcoded set of user indices $\vec{T}$, the $\texttt{test}$ operation may only be queried on $\vec{T}$ or subsets of $\vec{T}$, the $\texttt{reg-cor}$ and $\texttt{extract}$ operations may never be queried on any index in $\vec{T}$, the $\texttt{reveal}$ operation may never be queried on $\vec{T}$ or any subset of $\vec{T}$, and the above probability statement is neglibible for all values of $\vec{T}$ such that $\vec{T}\subseteq[\partycount]$ and $|\vec{T}| = g$.
\end{itemize}
\end{definition}

\nomiconimpliedby{\Dh (for two honest parties)~\cite{DH76}, \Cbdh (for three honest parties)~\cite{Jou00}, \Factoring in the \ROM (for two parties with adaptive security)~\cite{FHKP13}, \Dbdh (for two parties with adaptive security)~\cite{FHKP13}, \Io\s and \PRF\s and \Dss\s (for any party count with semi-static security)~\cite{BZ14}}