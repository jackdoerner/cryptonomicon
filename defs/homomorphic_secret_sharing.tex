\begin{definition}[\Homss~\cite{BGI16,BGILT18}] \label{def:homss}

A \Homss Scheme (\HOMSS) is a \secretsharing scheme that allows functions to evaluated homomorphically over secret-shared data.

Formally, a \homss scheme is parameterized by a client-count $\partycount$, a server-count $m$, a threshold $t$, and a function family $\mathcal{F}$ is a triple of \ppt algorithms, $(\mathsf{Share},\mathsf{Eval},\mathsf{Dec})$, such that
\begin{enumerate}
	\item Given a security parameter $\secparam$, a client index $i\in[\partycount]$, and a value $x$ in some domain $\mathcal{D}$, $\mathsf{Share}_\mathcal{D}$ produces $m$ shares, $$\{x^1,\ldots,x^m\}\drawnfrom\mathsf{Share}_\mathcal{D}(1^\secparam,i,x)$$
	\item Given the description of some function $f\in\mathcal{F}$, a server index $j\in[m]$, a common server input $x_0$, and the $j^\text{th}$ server's shares of the inputs for each of the $\partycount$ clients, $x_1^j,\ldots,x_\partycount^j$, the $\mathsf{Eval}$ function produces a value $y^j$ corresponding to the $j^\text{th}$ secret share of $f(x_0,x_1,\ldots,x_\partycount)$: $$y^j\assign\mathsf{Eval}_f(j,x_0,\{x_i^j\}_{i\in[\partycount]})$$ If the inputs to $\mathsf{Eval}$ are ill-formed, then $\mathsf{Eval}$ produces $\bot$.
	\item Given $m$ shares, $\{y^1,\ldots,y^m\}$ that were produced by the $\mathsf{Eval}_f$ function on $x_0$ and some consistent set of shares of $x_1,\ldots,x_\partycount$, if $f$ produces values in the domain $\mathcal{D}$, then $\mathsf{Dec}_\mathcal{D}$ reconstructs the value $f(\{x_i\}_{i\in[0,\partycount]})$.
\end{enumerate}

A \homss scheme must conform to two properties

\begin{enumerate}
	\item Correctness: for every $f:\mathcal{D}_0\times\mathcal{D}_1\times\ldots\times\mathcal{D}_\partycount\to\mathcal{D}_f$ that is a member of $\mathcal{F}$, and every value of $x_i\in\mathcal{D}_i$ for $i\in[0,\partycount]$ it holds that
	$$
	\Pr\left[\begin{aligned}
		&\mathsf{Dec}_{\mathcal{D}_f}(\{y^j\}_{j\in[m]})=f(\{x_i\}_{i\in[0,\partycount]}) : \\
		&\text{\qquad} \left\{\{x^j_i\}_{j\in[m]}\right\}_{i\in[\partycount]} \drawnfrom \left\{\mathsf{Share}_{\mathcal{D}_i}(1^\secparam,i,x_i)\right\}_{i\in[\partycount]}\\
		&\text{\qquad} \{y^j\}_{j\in[m]} \assign \left\{\mathsf{Eval}_f(j,x_0,\{x_i^j\}_{i\in[\partycount]})\right\}_{j\in[m]}
	\end{aligned}\right]=1-\mu(\secparam)
	$$
	If $\mu(\secparam)\in\negl(\secparam)$ then the scheme is \emph{statistically} correct, and if $\mu(\secparam)=0$, then it is \emph{perfectly} correct.
	\item Security: A \HOMSS scheme for function class $\mathcal{F}$ is said to be secure against $t$ corruptions for $t<m$ if and only if for all two-part \PPT adversaries $\mathcal{A}=(\mathcal{A}_1,\mathcal{A}_2)$ that pass state $s$ between them and every domain $\mathcal{D}$ associated with at least one function in $\mathcal{F}$, it holds that
	$$\left|\Pr\left[\begin{aligned}
		&\mathcal{A}_2\left(\{x^j\}_{j\in T},s\right)=b:\\
		&\text{\quad}(i,x_0,x_1)\drawnfrom\mathcal{A}_1(1^\secparam)\\
		&\text{\qquad}\suchthat i\in[\partycount], x_0\in\mathcal{D}, x_1\in\mathcal{D}\\
		&\text{\qquad}\land T\subset[m]\land|T|=t,\\
		&\text{\quad}b\drawnfrom\bit,\{x^j\}_{j\in[\partycount]}\drawnfrom\mathsf{Share}_{\mathcal{D}}(1^\secparam,i,x_b)
	\end{aligned}\right]-\frac{1}{2}\right|\in\negl(\secparam)$$
	That is, the adversary is allowed to choose up to $t$ of the parties to corrupt, and two input values $x_0$ and $x_1$ from the domain $\mathcal{D}$. The \game then chooses to create a secret sharing of one of the adversary's inputs at random, and the adversary receives the shares for the corrupted parties it has chosen. We insist that the adversary has no better than negligible advantage in determining whether it has received shares of $x_0$ or $x_1$, given this setup.
\end{enumerate}

In addition to the above algorithms, a \HOMSS scheme may also include a $\mathsf{Setup}$ algorithm that takes the security parameter and produces either a \crs, which is required by all other algorithms in order to achieve correctness, or a tuple $(\pk,\ek_1,\ldots,\ek_m)$ of a public key, which is required by the $\mathsf{Share}$ algorithtm, and $m$ evaluation keys, such that $\ek_j$ is required by $\mathsf{Eval}$ when it is called with server index $j$. In the latter case, the scheme is known as a \Pkhomss Scheme.

In addition to the above properties, a \HOMSS scheme might (but is not required to) achieve one or more of the following desirable properties:
\begin{enumerate}
	\setcounter{enumi}{2}
	\item Additivity: A \HOMSS scheme is said to be \emph{additive} if the $\mathsf{Dec}$ algorithm only computes addition in some abelian group. The group over which $\mathsf{Dec}$ computes may depend upon the function that has been evaluated. Note that additivity impies the following two properties.
	\item Compactness: A \HOMSS scheme is said to be \emph{compact} if there exists some specific polynomial $p$ such that for every $f:\mathcal{D}_0\times\mathcal{D}_1\times\ldots\times\mathcal{D}_\partycount\to\mathcal{D}_f$ that is a member of $\mathcal{F}$, and every value of $x_i\in\mathcal{D}_i$ for $i\in[0,\partycount]$, it holds that
	$$
	\Pr\left[\begin{aligned}
		&\bigwedge\limits_{j\in[m]}\left(\left|y^j\right|=p(\secparam)\cdot\left|f(\{x_i\}_{i\in[0,\partycount]})\right|\right) : \\
		&\text{\qquad} \left\{\{x^j_i\}_{j\in[m]}\right\}_{i\in[\partycount]} \drawnfrom \left\{\mathsf{Share}_{\mathcal{D}_i}(1^\secparam,i,x_i)\right\}_{i\in[\partycount]}\\
		&\text{\qquad} \{y^j\}_{j\in[m]} \assign \left\{\mathsf{Eval}_f(j,x_0,\{x_i^j\}_{i\in[\partycount]})\right\}_{j\in[m]}
	\end{aligned}\right]=1
	$$
	That is, the size of the shares produced by $\mathsf{Eval}$ has an overhead factor at most polynomial in the security parameter, with respect to the size of the cleartext output of the evaluated function. Note that additive schemes achieve compactness with $p(\secparam) = 1$.
	\item Weak Compactness: A \HOMSS scheme is said to be \emph{weakly compact} if there exists some specific polynomial $p$ and some sublinear function $g(\ell)\in o(\ell)$ such that for every $f:\mathcal{D}_0\times\mathcal{D}_1\times\ldots\times\mathcal{D}_\partycount\to\mathcal{D}_f$ that is a member of $\mathcal{F}$, and every value of $x_i\in\mathcal{D}_i$ for $i\in[0,\partycount]$, it holds that
	$$
	\Pr\left[\begin{aligned}
		&\bigwedge\limits_{j\in[m]}\left(\left|y^j\right|= g\left(\left|\{x_i\}_{i\in[0,\partycount]}\right|\right) + p(\secparam)\cdot\left|f(\{x_i\}_{i\in[0,\partycount]})\right|\right) : \\
		&\text{\qquad} \left\{\{x^j_i\}_{j\in[m]}\right\}_{i\in[\partycount]} \drawnfrom \left\{\mathsf{Share}_{\mathcal{D}_i}(1^\secparam,i,x_i)\right\}_{i\in[\partycount]}\\
		&\text{\qquad} \{y^j\}_{j\in[m]} \assign \left\{\mathsf{Eval}_f(j,x_0,\{x_i^j\}_{i\in[\partycount]})\right\}_{j\in[m]}
	\end{aligned}\right]=1
	$$
	That is, the size of the shares produced by $\mathsf{Eval}$ may exceed the bound established by the compactness property, so long as it is sublinear in the collective size of the cleartext inputs. Note that compact \HOMSS schemes achieve weak compactness with $g(\ell) = 0$.
	\item Robustness: A \HOMSS scheme is said to be \emph{robust} if the $\mathsf{Dec}$ algorithm requires only $t+1$ shares in order to reconstruct the output. Formally, for every $f:\mathcal{D}_0\times\mathcal{D}_1\times\ldots\times\mathcal{D}_\partycount\to\mathcal{D}_f$ that is a member of $\mathcal{F}$, every value of $x_i\in\mathcal{D}_i$ for $i\in[0,\partycount]$, and every $T'\subset[m]$ such that $|T'| > t$ it must hold that
	$$
	\Pr\left[\begin{aligned}
		&\mathsf{Dec}_{\mathcal{D}_f}(\{y^j\}_{j\in T'})=f(\{x_i\}_{i\in[0,\partycount]}) : \\
		&\text{\qquad} \left\{\{x^j_i\}_{j\in[m]}\right\}_{i\in[\partycount]} \drawnfrom \left\{\mathsf{Share}_{\mathcal{D}_i}(1^\secparam,i,x_i)\right\}_{i\in[\partycount]}\\
		&\text{\qquad} \{y^j\}_{j\in[m]} \assign \left\{\mathsf{Eval}_f(j,x_0,\{x_i^j\}_{i\in[\partycount]})\right\}_{j\in[m]}
	\end{aligned}\right]=1-\mu(\secparam)
	$$
\end{enumerate}
\end{definition}

\nomicondiscussion{\Homss is in some sense a dual notion to \Fss: whereas \HOMSS shares inputs and allows public functions to be computed homomorphically over the sharings, \FSS shares functions and allows their sharings to be applied in a distributed, non-interactive fashion to public inputs.}

\nomiconimpliedby{\DDH (for branching programs, one client and two servers, with additivity)~\cite{BGI16}, \LWE (for circuits with any number of servers and clients, with additivity)~\cite{BGILT18}}