\begin{definition}[\Ciphertextnm~\cite{DDN91}]\label{def:ciphertextnm}
\Ciphertextnm is a property of an \enc scheme which states that ciphertexts produced by the \enc scheme are non-malleable to all \PPT adversaries, even when those adversaries have oracle access to some part of the \enc scheme. \ciphertextnm is typically considered in the context of \cpa\s, \ccaone\s, or \ccatwo\s.

Formally, suppose there exists an \enc scheme with a key-generator $\mathsf{Gen}$, an encryption algorithm $\mathsf{Enc}$ over a message space $\mathcal{M}$, and a decryption algorithm $\mathsf{Dec}$ such that:
$$\left(\pk,\sk\right) \drawnfrom \mathsf{Gen}(1^\secparam)\text{\qquad\qquad}c \drawnfrom \mathsf{Enc}_{\pk}(m)\text{\qquad\qquad}m\assign\mathsf{Dec}_{\sk}(c)$$
where $1^\secparam$ is a security parameter, $\pk$ and $\sk$ are the public and secret keys, respectively (these may be the same if the scheme is a \symenc), $m\in\mathcal{M}$ is any message in the message space, and $c$ is a ciphertext. For all \PPT adversaries $\mathcal{A}=\left(\mathcal{A}_1,\mathcal{A}_2\right)$ (comprising two algorithms which pass the state $s$ between them), such that $\mathcal{A}_1$ has access to an \oracle $\mathcal{O}_1$ and $\mathcal{A}_2$ has access to an \oracle $\mathcal{O}_2$, where $\vec{q}$ is the set of queries that $\mathcal{A}_2$ asks $\mathcal{O}_2$, suppose that
$$\left|\begin{aligned}
	& \Pr\left[\begin{aligned}
		& R(m,m')=1 \land c'\ne c \land c \notin \vec{q}:\\
		& \text{\quad} (\pk,\sk)\drawnfrom\mathsf{Gen}(1^\secparam), (M,s)\drawnfrom\mathcal{A}_1^{\mathcal{O}_1}(1^\secparam),\\
		& \text{\quad} m\drawnfrom M, c\drawnfrom \mathsf{Enc}_{\pk}(m),\\
		& \text{\quad} (R,c') \drawnfrom \mathcal{A}_2^{\mathcal{O}_2}(1^\secparam,c,s), m'\assign\mathsf{Dec}_{\sk}(c')
	\end{aligned}\right] \\
	& - \Pr\left[\begin{aligned}
		& R(m,m')=1:\\
		& \text{\quad} (\pk,\sk)\drawnfrom\mathsf{Gen}(1^\secparam), (M,s)\drawnfrom\mathcal{A}_1^{\mathcal{O}_1}(1^\secparam,\pk),\\
		& \text{\quad} m\drawnfrom M, (R,c') \drawnfrom \mathcal{A}_2^{\mathcal{O}_2}(1^\secparam,s), m'\assign\mathsf{Dec}_{\sk}(c')
	\end{aligned}\right]
\end{aligned}\right|\in\negl(\secparam)$$
That is, the adversary receives \oracle access to $\mathcal{O}_1$, and, after performing a polynomial number of queries, it produces a description of some distribution $M$ over the message space. The \game draws a message $m$ from this distribution, encrypts it, and returns the ciphertext $c$ to the adversary, which may then perform a polynomial number of \oracle queries to $\mathcal{O}_2$ (but may not query the oracle with $c$). The adversary finally attempts to produce a new ciphertext $c'$ (corresponding to a message $m'$) and a relation $R$, such that $m$ and $m'$ satisfy $R$ (the adversary is forbidden to return the equality relation). We insist that the adversary must succeed with negligible advantage, relative to performing the same task without access to $c$. With respect to this \game, the following security properties are defined
\begin{enumerate}
	\item \NMCPA Security~\cite{DDN91}: If $\mathcal{O}_1 = \mathcal{O}_2 = \mathsf{Enc}_{\pk}(\hole)$  (i.e. the adversary has \oracle access to the encryption function) then the scheme is said to have \nmcpa.
	\item \NMCCA Security~\cite{DDN03}: If $\mathcal{O}_1 = \mathsf{Dec}_{\sk}(\hole)$ and $\mathcal{O}_2 = \mathsf{Enc}_{\pk}(\hole)$ (i.e. the adversary has \oracle access to the decryption function \emph{before} producing its message distribution, but not after) then the scheme is said to have \nmcca.
	\item \NMCCATWO Security~\cite{DDN03}; If $\mathcal{O}_1 = \mathcal{O}_2 = \mathsf{Dec}_{\sk}(\hole)$ (i.e. the adversary has \oracle access to the decryption function both before and after generating a message distribution) then the scheme is said to have \nmccatwo. Note that the adversary is forbidden to call the decryption oracle upon $c$.
\end{enumerate}
\end{definition}