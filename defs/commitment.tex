\begin{definition}[\Com~\cite{BCC88}] \label{def:com}
A \Com Scheme is a tuple of \PPT algorithms, $(\mathsf{Gen},\allowbreak\mathsf{Com},\allowbreak\mathsf{Open})$ such that:
\begin{enumerate}
	\item Given a security parameter $\secparam$, the $\mathsf{Gen}$ algorithm produces a commitment key: $k\drawnfrom\mathsf{Gen}(1^\secparam)$. Note that unlike in the case of (for example) \enc\s, this key is not assumed to be private.
	\item Given a key $k$ and a message $m$, the $\mathsf{Com}$ algorithm outputs a commitment $c$ and and opening $d$: $(c,d) \drawnfrom \mathsf{Com}_k(m)$
	\item Given a key $k$, a commitment $c$, and an opening $d$, the $\mathsf{Open}$ algorithm outputs $\bot$ if the $(c,d)$ pair is invalid, or the message corresponding to the commitment if the $(c,d)$ pair is valid: $m \assign \mathsf{Open}_k(c,d)$
\end{enumerate}
A \com scheme is required to conform to two properties:
\begin{enumerate}
	\item Hiding: The commitment $c$ must reveal negligible information about the message to which it corresponds. Specifically, for all \PPT adversaries $\mathcal{A}=\left(\mathcal{A}_1,\mathcal{A}_2\right)$ (comprising two algorithms which pass the state $s$ between them), $\mathcal{A}_1$ is given access to the commitment key $k$, and attempts to produce two messages $m_0,m_1$ for which $\mathcal{A}_2$ can distinguish the corresponding commitments. If the adversary can distinguish those commitments with no better than negligible advantage, then the scheme is said to be \emph{computationally hiding}. If the property holds when the adversary is computationally unbounded, the scheme is said to be \emph{statistically} hiding. If a computationally unbounded adversary has exactly zero advantage (i.e. every message has an equal probability of correspondence to every commitment in the commitment space), then the scheme is said to be \emph{perfectly} hiding. Formally,
	$$\left|\Pr\left[\begin{aligned}
		&\mathcal{A}_2(c,s) = b :\\
		&\text{\quad}k\drawnfrom\mathsf{Gen}(1^\secparam),(m_0,m_1,s)\drawnfrom\mathcal{A}_1(k),\\
		&\text{\quad}b\drawnfrom\uniformdist{\bit},(c,d)\drawnfrom\mathsf{Com}_k(m_b)
	\end{aligned}\right] - \frac{1}{2}\right|\in\negl(\secparam)$$
	\item Binding: Given a commitment $c$, it is not possible to efficiently find two openings $d,d'$ such that the commitment opens to two different messages $m,m'$. Formally, for all \PPT adversaries $\mathcal{A}$,
	$$\Pr\left[\begin{aligned}
		&m \ne m' \land m,m'\ne\bot:\\
		&\text{\quad}k\drawnfrom\mathsf{Gen}(1^\secparam),(c,d,d')\drawnfrom\mathcal{A}(k),\\
		&\text{\quad}m\assign\mathsf{Open}_k(c,d),m'\assign\mathsf{Open}_k(c,d')
	\end{aligned}\right]\in\negl(\secparam)$$
	If the above property holds, the scheme is said to be \emph{computationally binding}. If it holds when the adversary is computationally unbounded, then the scheme is said to be \emph{statistically} binding. If the advantage of a computationally unbounded adversary is exactly zero (i.e. there is a one-to-one correspondence between valid messages and valid commitments), then the cheme is said to be \emph{perfectly} binding.
\end{enumerate}
\end{definition}

\nomicondiscussion{Note that no scheme can be both perfectly hiding and perfectly binding simultaneously.}

\nomiconimpliedby{\CRHF, \DL~\cite{Ped91}}