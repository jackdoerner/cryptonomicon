\begin{definition}[\Garbling~\cite{BHR12}] \label{def:garbling}
A \Garbling Scheme is a tuple of algorithms $(\mathsf{Gb},\mathsf{En},\mathsf{De},\mathsf{Ev})$ such that
\begin{enumerate}
	\item $\mathsf{Gb}$ is a \PPT function that takes a security parameter $\secparam$ and a description $f$ of the function to be garbled and produces a tuple containing the garbled representation $F$ of $f$, along with some encoding and decoding information: $(F,e,d)\drawnfrom\mathsf{Gb}(1^\secparam,f)$.
	\item Given the encoding information $e$ and an input $x$ that is compatible with $f$, $X\assign\mathsf{En}(e,x)$ deterministically produces a garbled input $X$ that is compatible with $F$.
	\item Given a garbled function $F$ and a garbled input $X$, $Y\assign\mathsf{Ev}(F,X)$ deterministically computes a garbled output $Y$ if $F$ and $X$ are jointly in the image of the garbling scheme, or $\bot$ otherwise.
	\item Given a garbled output $Y$ and the decoding information $d$, $y\assign\mathsf{De}(d,Y)$ deterministically computes the plain output $y$ if $d$ and $Y$ are jointly in the image of the garbling scheme, or $\bot$ otherwise.
\end{enumerate}
A \garbling scheme is \emph{correct} if and only if for all security parameters $\secparam$, all functions $f$ compatible with the scheme and all inputs $x$ compatible with $f$ it holds that $$\Pr\left[\begin{aligned}&f(x)=y:\\&\quad (F,e,d)\drawnfrom\mathsf{Gb}(1^\secparam,f), X\assign\mathsf{En}(e,x), Y\assign\mathsf{Ev}(F,X), y\assign\mathsf{De}(d,Y)\end{aligned}\right]=1$$

In addition to correctness, there are three possible security properties, which are defined relative to a \emph{leakage} or \emph{side-information} function $\phi$, which operates on the function description $f$ (but \emph{not} the inputs). A secure garbling scheme must achieve one or more of the following properties:
\begin{enumerate}
	\item Obliviousness: There exists a simulator $\mathcal{S}$ that takes as input the leakage $\phi(f)$ and produces some output that cannot be efficiently distinguished from an adversarially chosen garbled function with adversarially chosen garbled inputs. Formally, a \garbling scheme is simulation-oblivious if there exists a \PPT algorithm $\mathcal{S}$ such that for all two-part \PPT adversaries $(\mathcal{A}_1,\mathcal{A}_2)$ that communicate by passing some state $s$, and all security parameters $\secparam$,
	$$\left|\begin{aligned}
		&\Pr\left[\begin{aligned}
			&\mathcal{A}_2(F,X,s)=1 :\quad(f,x,s)\drawnfrom\mathcal{A}_1(1^\secparam),\\
			&\qquad(F,e,d)\drawnfrom\mathsf{Gb}(1^\secparam,f),~
			X\assign\mathsf{En}(e,x)
		\end{aligned}\right]\\
		&\qquad-\Pr\left[\begin{aligned}
			&\mathcal{A}_2(F,X,s)=1 :\quad(f,x,s)\drawnfrom\mathcal{A}_1(1^\secparam),\\
			&\qquad(F,X)\drawnfrom\mathcal{S}(1^\secparam,\phi(f))
		\end{aligned}\right]
	\end{aligned}\right|\in\negl(\secparam)$$

	\item Privacy: There exists a simulator $\mathcal{S}$ that takes as input the leakage $\phi(f)$ \emph{and} an output $y=f(x)$ and produces simulated decoding information $d$ in addition to $(F,X)$. Formally, a \garbling scheme is simulation-private if there exists a \PPT algorithm $\mathcal{S}$ such that for all two-part \PPT adversaries $(\mathcal{A}_1,\mathcal{A}_2)$ that communicate by passing some state $s$, and all security parameters $\secparam$,
	$$\left|\begin{aligned}
		&\Pr\left[\begin{aligned}
			&\mathcal{A}_2(F,X,d,s)=1 :\quad(f,x,s)\drawnfrom\mathcal{A}_1(1^\secparam),\\
			&\qquad(F,e,d)\drawnfrom\mathsf{Gb}(1^\secparam,f),~
			X\assign\mathsf{En}(e,x)
		\end{aligned}\right]\\
		&\qquad-\Pr\left[\begin{aligned}
			&\mathcal{A}_2(F,X,d,s)=1 :\quad(f,x,s)\drawnfrom\mathcal{A}_1(1^\secparam),\\
			&\qquad(F,X,d)\drawnfrom\mathcal{S}(1^\secparam,\phi(f),f(x))
		\end{aligned}\right]
	\end{aligned}\right|\in\negl(\secparam)$$
	
	\item Authenticity: Given a garbled function with garbled inputs corresponding to a function and inputs of its choosing, no efficient adversary can produce a garbled output that corresponds to other functions or inputs with better than negligible probability. Formally, a \garbling scheme is authentic if for all two-part \PPT adversaries $(\mathcal{A}_1,\mathcal{A}_2)$ that communicate by passing some state $s$, and all security parameters $\secparam$,
	$$\Pr\left[\begin{aligned}
		&\mathsf{De}(d,Y)\neq\bot\land\mathsf{Ev}(F,X)\neq Y : \\
		&\qquad(f,x,s)\drawnfrom\mathcal{A}_1(1^\secparam),~~(F,e,d)\drawnfrom\mathsf{Gb}(1^\secparam,f),\\
		&\qquad X\assign\mathsf{En}(e,x),~~Y\drawnfrom\mathcal{A}_2(F,X,s)
	\end{aligned}\right]\in\negl(\secparam)$$
\end{enumerate}
\end{definition}

\nomicondiscussion{The origin of garbling (and of secure computation in general) is often given as one of two papers by Yao~\cite{Yao82a,Yao86}. Neither paper describes the scheme now known as Yao's Garbled Circuits; he introduced the scheme during oral presentations of those works, and it seems to have been first written down by Goldreich et al.~\cite{GMW87}. Bellare et al.~\cite{BHR12} formulated the above definitions much later, and also gave indistinguishability-based versions of the obliviousness and privacy properties, but proved that they are equivalent to the simulation-based properties for circuit garbling schemes that leak the size or topology of the circuit. Note that the original definition of Bellare et al. includes an additional algorithm $\mathsf{ev}$ that evaluates a non-garbled function $f$ from its description; here instead the notation is abused such that $f$ represents both a function \emph{and} its description.}

\nomiconimpliedby{\OWF (for boolean circuits with all three security properties and leakage of topology)~\cite{LP09}, \OWF (for RAM machines with all three security properties and leakage of padded runtime)~\cite{GLOS15}, \CCRH (for arithmetic circuits with all three security properties and leakage of topology)~\cite{BMR16}, \CCRH (for one-hot circuits with all three security properties and leakage of topology)~\cite{HK21}}