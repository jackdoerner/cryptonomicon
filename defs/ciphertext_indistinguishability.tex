\begin{definition}[\Ciphertextindist~\cite{GM82}]\label{def:ciphertextindist}
\Ciphertextindist is a property of an \enc scheme which states that ciphertexts produced by the \enc scheme are indistinguishable to all \PPT adversaries, even when those adversaries have oracle access to some part of the \enc scheme. \ciphertextindist is typically considered in the context of \cpa\s, \ccaone\s, or \ccatwo\s.

Formally, suppose there exists an \enc scheme with a key-generator $\mathsf{Gen}$, an encryption algorithm $\mathsf{Enc}$ over a message space $\mathcal{M}$, and a decryption algorithm $\mathsf{Dec}$ such that:
$$\left(\pk,\sk\right) \drawnfrom \mathsf{Gen}(1^\secparam)\text{\qquad\qquad}c \drawnfrom \mathsf{Enc}_{\pk}(m)\text{\qquad\qquad}m\assign\mathsf{Dec}_{\sk}(c)$$
where $1^\secparam$ is a security parameter, $\pk$ and $\sk$ are the public and secret keys, respectively (these may be the same if the scheme is a \symenc), $m\in\mathcal{M}$ is any message in the message space, and $c$ is a ciphertext. For all \PPT adversaries $\mathcal{A}=\left(\mathcal{A}_1,\mathcal{A}_2\right)$ comprising two algorithms which pass the state $s$ between them, such that $\mathcal{A}_1$ has access to an \oracle $\mathcal{O}_1$ and $\mathcal{A}_2$ has access to an \oracle $\mathcal{O}_2$, where $\vec{q}$ is the set of queries that $\mathcal{A}_2$ asks $\mathcal{O}_2$, suppose that
$$\left|\Pr\left[\begin{aligned}
	& \mathcal{A}_2^{\mathcal{O}_2}(1^\secparam,c,s)=b \land c \notin \vec{q}:\\
	& \text{\quad} (\pk,\sk)\drawnfrom\mathsf{Gen}(1^\secparam), (m_0,m_1,s)\drawnfrom\mathcal{A}_1^{\mathcal{O}_1}(1^\secparam),\\
	& \text{\quad} b\drawnfrom\uniformdist{\bit}, c\drawnfrom \mathsf{Enc}_{\pk}(m_b)
\end{aligned}\right] - \frac{1}{2}\right|\in\negl(\secparam)$$
That is, the adversary receives \oracle access to $\mathcal{O}_1$, and, after performing a polynomial number of queries, it produces two challenge messages. The \game encrypts one of these two messages at random and returns the ciphertext $c$ to the adversary, which may then perform a polynomial number of \oracle queries to $\mathcal{O}_2$ (but may not query the oracle with $c$) before guessing which of its messages was encrypted to produce $c$. We insist that the adversary must be correct with negligible advantage, relative to guessing at random. With respect to this \game, the following security properties are defined
\begin{enumerate}
	\item \INDCPA Security~\cite{GM82}: If $\mathcal{O}_1 = \mathcal{O}_2 = \mathsf{Enc}_{\pk}(\hole)$  (i.e. the adversary has \oracle access to the encryption function) then the scheme is said to have \indcpa.
	\item \INDCCA Security~\cite{NY90}: If $\mathcal{O}_1 = \mathsf{Dec}_{\sk}(\hole)$ and $\mathcal{O}_2 = \mathsf{Enc}_{\pk}(\hole)$ (i.e. the adversary has \oracle access to the decryption function \emph{before} producing its challenge messages, but not after) then the scheme is said to have \indcca.
	\item \INDCCATWO Security~\cite{RS92}: If $\mathcal{O}_1 = \mathcal{O}_2 = \mathsf{Dec}_{\sk}(\hole)$ (i.e. the adversary has \oracle access to the decryption function both before and after generating its challenge messages) then the scheme is said to have \indccatwo. Note that the adversary is forbidden to call the decryption oracle upon $c$.
\end{enumerate}
\end{definition}


