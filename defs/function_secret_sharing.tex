\begin{definition}[\Fss~\cite{BGI15}] \label{def:fss}

A \Fss Scheme (\FSS) is a \secretsharing scheme that allows the description of a function $f$ from a function class $\mathcal{F}$ to be secret-shared among a set of players in such a way that they can jointly evaluate $f$ at any point in its domain to produce shares of its output, while learning nothing about the function or the \emph{reconstruction} of its output apart from the fact that it belongs to $\mathcal{F}$.

Formally, a \fss scheme for $\partycount$ parties and a function family $\mathcal{F}$ is a triple of \ppt algorithms, $(\mathsf{Gen},\mathsf{Eval},\mathsf{Dec})$, such that
\begin{enumerate}
	\item Given a security parameter $\secparam$ and a description of function $f$ from the family $\mathcal{F}$, $\mathsf{Gen}$ produces $\partycount$ keys, $$\left\{k_1,\ldots,k_\partycount\right\}\drawnfrom\mathsf{Gen}(1^\secparam,f)$$
	\item Given a party index $i$, the matching party key $k_i$, and a value $x$ in the domain of $f$, $\mathsf{Eval}$ produces a value $y_i$ corresponding to the $i^\text{th}$ share of $f(x)$ in a $\partycount$-party secret sharing scheme
	$$y_i\assign\mathsf{Eval}(i,k_i,x)$$
	\item Given $\partycount$ shares, $\left\{y_1,\ldots,y_\partycount\right\}$ such that for $i\in[\partycount]$, $y_i=\mathsf{Eval}(i,k_i,x)$ for some consistent value $x$, $\mathsf{Dec}$ reconstructs the value $f(x)$
\end{enumerate}

A \fss scheme must conform to two properties

\begin{enumerate}
	\item Correctness: Given a set of correctly generated keys for any function $f\in\mathcal{F}$ and any value $x\in\mathsf{Domain}(f)$, the output shares produced by the $\mathsf{Eval}$ algorithm on each of the keys and $x$ should decode to produce $f(x)$. Formally, for all $\secparam\in\NN$, $\partycount\in\NN$, $f\in\mathcal{F}$, and $x\in\mathsf{Domain}(f)$,
	$$\Pr\left[\begin{aligned}
		&\mathsf{Dec}\left(\left\{y_i\right\}_{i\in[\partycount]}\right)=f(x):\\
		&\text{\quad}\left\{k_i\right\}_{i\in[\partycount]}\drawnfrom\mathsf{Gen}(1^\secparam,f),\\
		&\text{\quad}\left\{y_i\right\}_{i\in[\partycount]}\assign\left\{\mathsf{Eval}\left(i,k_i,x\right)\right\}_{i\in[\partycount]}
	\end{aligned}\right]=1$$
	\item Security: A \fss scheme is said to be secure against $t$ corruptions for $t<\partycount$ if and only if for all two-part \PPT adversaries $\mathcal{A}=(\mathcal{A}_1,\mathcal{A}_2)$ that pass state $s$ between them, it holds that
	$$\left|\Pr\left[\begin{aligned}
		&\mathcal{A}_2\left(\left\{k_i\right\}_{i\in T},s\right)=b:\\
		&\text{\quad}(f_0,f_1,T,s)\drawnfrom\mathcal{A}_1(1^\secparam)\\
		&\text{\qquad}\suchthat \mathsf{Domain}(f_0)=\mathsf{Domain}(f_1)\\
		&\text{\qquad}\land T\subset[\partycount]\land|T|=t,\\
		&\text{\quad}b\drawnfrom\bit,\left\{k_i\right\}_{i\in[\partycount]}\drawnfrom\mathsf{Gen}(1^\secparam,f_b)
	\end{aligned}\right]-\frac{1}{2}\right|\in\negl(\secparam)$$
	That is, the adversary is allowed to choose up to $t$ of the parties to corrupt, and two functions $f_0$ and $f_1$ with identical domains. The \game then chooses to create a secret sharing of one of the adversary's functions at random, and the adversary receives the keys for the corrupted parties it has chosen. We insist that the adversary has no better than negligible advantage in determining whether it has received a sharing of $f_0$ or $f_1$, given this setup.
\end{enumerate}
\end{definition}

\nomicondiscussion{\Fss is in some sense a dual notion to \Homss: whereas \FSS shares functions and allows their sharings to be applied in a distributed, non-interactive fashion to public inputs, \HOMSS shares inputs and allows public functions to be computed homomorphically over the sharings.}

\nomiconimpliedby{\OWF (for point functions)~\cite{BGI15}, \IO and \OWF (for all functions)~\cite{BGI15}}