\begin{assumption}[\Lpn~\cite{BFKL93}]\label{def:lpn}
\Lpn (\LPN) is a cryptographic problem over \lattice\s and learning algorithms that is assumed to be intractable for any \PPT algorithm. It is closely related to the \lpn problem. Instances of the \LPN problem are generated by a \PPT algorithm $$\left(\vec{s},n,m,\tau\right) \drawnfrom \mathsf{LPNGen}(1^\secparam)$$ which takes a security parameter $\secparam$ and produces an integer $n$, a vector $\vec{s}\in\bit^n$ along with a sample count $m\in\poly(n)$ and a parameter $\tau\in\RR$ such that $0 < \tau < 0.5$. Let $\mathsf{Ber}_\tau$ denote the Bernoulli distribution with parameter $\tau$. Both variants of the problem are now stated in the form of assumptions about their difficulty:

\begin{enumerate}
	\item The \Dlpn (\DLPN) assumption asserts that there exists an algorithm $\mathsf{LPNGen}$ such that if $$\left(\vec{s},n,m,\tau\right) \drawnfrom \mathsf{LPNGen}(1^\secparam), \vec{A}\drawnfrom\bit^{m\times n}, \vec{e}\drawnfrom \mathsf{Ber}_\tau^m, \vec{b}\drawnfrom\bit^m$$
		then $$\left(\vec{A},\vec{A}\vec{s}\xor\vec{e}\right)\compequiv\left(\vec{A},\vec{b}\right)$$
		In other words, $m$ random linear combinations of the elements of $\vec{s}$ with errors $\vec{e}$ cannot be efficiently distinguished from $m$ uniform samples (given by $\vec{b}$), even when the matrix $\vec{A}$ that generated the combinations is known.
	\item The \Slpn (\SLPN) assumption asserts that there exists an algorithm $\mathsf{LPNGen}$ such that for all \PPT adversarial algorithms $\mathcal{A}$,
		$$\Pr\left[\begin{aligned}
			&\mathcal{A}(\vec{A},\vec{A}\vec{s}\xor\vec{e})=\vec{s}:\\
			&\text{\quad} \left(\vec{s},n,m,\tau\right) \drawnfrom \mathsf{LPNGen}(1^\secparam),\\
			&\text{\quad} \vec{A}\drawnfrom\bit^{m\times n}, \vec{e}\drawnfrom \mathsf{Ber}_\tau^m
		\end{aligned}\right]\in\negl(\secparam)$$
		In other words, given $m$ random linear combinations of the elements of $\vec{s}$ with errors $\vec{e}$ and the matrix $\vec{A}$ used to generate the combinations, $\vec{s}$ cannot be efficiently calculated.
\end{enumerate}

The \emph{worst-case} version of the \LPN assumption states that there exists a distribution from which $\vec{s}$ can be sampled such that the above probabilities hold. The \emph{average-case} version states that there exists a choice of the other parameters such that the above probabilities hold when $\vec{s}$ is sampled \emph{uniformly} from $\bit^n$. It is known that the search and decision variants of the \LPN problem are equivalent~\cite{BFKL93}.
\end{assumption}

\nomicondiscussion{It is usually the case that $n=\secparam$; we have kept the two separate for clarity.}