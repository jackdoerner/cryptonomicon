.PHONY: all clean

DEPENDS=$(shell ls *.tex) $(shell ls asmps/*.tex defs/*.tex) references.bib
NAME=cryptonomicon
all: $(NAME).pdf

$(NAME).pdf: $(NAME).tex $(DEPENDS)
	pdflatex --shell-escape -f --pdf --warn $<
	bibtex $(NAME)
	pdflatex --shell-escape -f --pdf --warn $<
	pdflatex --shell-escape -f --pdf --warn $<

clean:
	rm -rf *.aux *.bbl *.blg *.log *.pdf *.toc *.snm *.out *.nav *.dvi tags auto
